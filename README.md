This is an example MkDocs plugin.

The plugin replaces the word "hello" with "goodbye" in all pages.

Run `mkdocs serve` and see if the contents at [index.md](docs/index.md) change.

Quick setup:

```bash
python -m venv .venv
source .venv/bin/activate
pip install mkdocs
pip install -e .
mkdocs serve
```

Reading magerial:

- https://github.com/byrnereese/mkdocs-plugin-template
- https://www.mkdocs.org/dev-guide/plugins/
- This conversation: https://chat.openai.com/share/bb30e79f-779d-4e2c-a982-47b98f04109d
