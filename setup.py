from setuptools import setup, find_packages

setup(
    name='replace_hello',
    version='0.1.0',
    packages=find_packages(),
    entry_points={
        'mkdocs.plugins': [
            'replace_hello = replace_hello:ReplaceHelloPlugin'
        ]
    },
    install_requires=[
        'mkdocs>=1.0',
    ],
)
