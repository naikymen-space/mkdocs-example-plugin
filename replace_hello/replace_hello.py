from mkdocs.plugins import BasePlugin
from mkdocs.exceptions import PluginError

class ReplaceHelloPlugin(BasePlugin):
    def on_page_content(self, html, page, config, files, **kwargs):
        # Replace occurrences of "hello" with "goodbye"
        print("Hi!!")
        modified_html = html.replace("hello", "goodbye")
        return modified_html
